﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web
{
    public partial class Carrito : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] == null)
            {
                Response.Redirect("Inicio.aspx");
            }
            else
            {
                string usuario = Session["user"].ToString();
                this.Master.LabelMensaje = "Bienvenido: " + usuario;
                this.Master.ButtonEnable = true;
                this.Master.ButtonEnableCarrito = true;

                ServiceReference1.Service1Client s = new ServiceReference1.Service1Client();
                GridView1.DataSource = s.ListarPedidoUsuario(usuario).ToList();
                GridView1.DataBind();
            }

        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            if (GridView1.Rows.Count > 0)
            {
                ServiceReference1.Service1Client s = new ServiceReference1.Service1Client();
                string us = Session["user"].ToString();
                int estado = s.EliminarPedido(Convert.ToInt32(txtEliminar.Text),us);
                if (estado == 1)
                {
                    s.EliminarPedido(Convert.ToInt32(txtEliminar.Text), us);
                    Response.Redirect(Request.RawUrl);
                }else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Ingrese una ID válida')", true);
                }
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Carrito Vacío')", true);
            }
        }

        protected void btnPagar_Click(object sender, EventArgs e)
        {
            if (GridView1.Rows.Count > 0)
            {
                if (DropDownList1.SelectedValue == "Despacho")
                {
                    ServiceReference1.Service1Client s = new ServiceReference1.Service1Client();
                    string usuario = Session["user"].ToString();
                    Response.Redirect("Pago.aspx?Retiro=" + DropDownList1.SelectedValue);
                }
                else if (DropDownList1.SelectedValue == "Tienda")
                {
                    ServiceReference1.Service1Client s = new ServiceReference1.Service1Client();
                    string usuario = Session["user"].ToString();
                    string direccion = s.DireccionUsuario(usuario);
                    Response.Redirect("Pago.aspx?Retiro=" + DropDownList1.SelectedValue);
                }
                else if (DropDownList1.SelectedValue == "Seleccionar")
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Seleccionar Despacho')", true);
                }
            }else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Carrito Vacío')", true);
            }
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
    }
}