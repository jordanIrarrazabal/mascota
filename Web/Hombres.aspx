﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Hombres.aspx.cs" Inherits="Web.Hombres" %>
<%@ MasterType VirtualPath="~/Site1.Master" %> 
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    .auto-style4 {
        width: 100%;
        height: 299px;
    }
        </style>
</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="ContentPlaceHolder2">
    
    
    <asp:GridView ID="GridView1" runat="server" ShowHeader="False" GridLines="None" AutoGenerateColumns = "False"
       Font-Names = "Arial" DataSourceID="SqlDataSource1" BorderStyle="None" Width="50%" HorizontalAlign="Center" DataKeyNames="Id">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#String.Format("detalleProducto.aspx?IdProducto={0}", Eval("id")) %>' Text="Ver Detalle" ></asp:HyperLink>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:ImageField DataImageUrlField="imagen" ControlStyle-Width="300">
<ControlStyle Width="300px"></ControlStyle>
        </asp:ImageField>
    </Columns>
    </asp:GridView>
    
    
    
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:vestiventaConnectionString %>" SelectCommand="SELECT [Id], [nombre], [imagen] FROM [producto] WHERE ([destino] = @destino)">
        <SelectParameters>
            <asp:Parameter DefaultValue="1" Name="destino" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    
    
    
</asp:Content>

