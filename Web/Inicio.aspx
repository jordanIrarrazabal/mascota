﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Inicio.aspx.cs" Inherits="Web.Inicio" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="https://dansup.github.io/bulma-templates/css/bulma.css">
  <link rel="stylesheet" type="text/css" href="https://dansup.github.io/bulma-templates/css/base.css">
    <div class="auto-style3">
        <section class="hero is-fullheight is-dark is-bold">
    <div class="hero-body">
      <div class="container">
        <div class="columns is-vcentered">
<div class="column is-4 is-offset-4">
       <div class="card">
  <div class="card-content">
    <p style="color:black;" class="title">
      !Bienvenido Querido Usuario a la mejor Tienda ON-line de la red!
    </p>
    <p style="color: #6d737c;" class="subtitle">
      VestiVenta
    </p>
  </div>
  <footer class="card-footer">
    <p class="card-footer-item">
      <span>
      <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Login.aspx">Iniciar Sesión</asp:HyperLink>
      </span>
    </p>
    <p class="card-footer-item">
      <span>
       <asp:HyperLink ID="HyperLink2" runat="server"  NavigateUrl="~/Registrar.aspx">Registrarse</asp:HyperLink>
      </span>
    </p>
  </footer>
</div>
    </div>
            </div>
          </div>
        </div>
            </div>
    </section>
</asp:Content>
<asp:Content ID="Content4" runat="server" contentplaceholderid="ContentPlaceHolder1">
        
            <table style="width:100%;">
                <tr>
                    <td colspan="3">
                        <div  class="auto-style4">
                            <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Verdana" Font-Overline="False" Font-Size="X-Large" ForeColor="White" Text="--->VestiVenta<---" style="float:left;background-color: black;"></asp:Label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">
                        <asp:Menu ID="Menu1" runat="server" DataSourceID="SiteMapDataSource2" ForeColor="Black" Orientation="Horizontal" Font-Bold="True" Font-Names="Verdana" Font-Size="Medium" StaticDisplayLevels="2" BackColor="White">
                            <DynamicHoverStyle BackColor="White" BorderColor="#FF3300" BorderStyle="Solid" BorderWidth="1px" />
                            <DynamicMenuItemStyle BackColor="White" />
                            <DynamicMenuStyle HorizontalPadding="30px" BackColor="White"/>
                            <DynamicSelectedStyle BackColor="White" />
                            <StaticMenuItemStyle HorizontalPadding="30px" VerticalPadding="15px" />
                            <StaticMenuStyle HorizontalPadding="30px"/>
                            
                        </asp:Menu>
                        <asp:SiteMapDataSource ID="SiteMapDataSource2" runat="server" />
                    </td>
                    <td class="auto-style3">
                        <asp:HyperLink ID="hpLogin" runat="server" CssClass="button is-success" NavigateUrl="~/Login.aspx">Iniciar Sesión</asp:HyperLink>
                    </td>
                    <td class="auto-style3">
                        <asp:HyperLink ID="hpRegister" runat="server" CssClass="button is-primary" NavigateUrl="~/Registrar.aspx">Registrarse</asp:HyperLink>
                    </td>
                </tr>
                </table>
        
        </asp:Content>



