﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Web.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Login</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" id="bulma" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.3.2/css/bulma.min.css" />
  <link rel="stylesheet" type="text/css" href="https://dansup.github.io/bulma-templates/css/login.css">
</head>
<body>
  <div class="login-wrapper columns">
    <div class="column is-8 is-hidden-mobile hero-banner">
      <section class="hero is-fullheight is-dark">
        <div class="hero-body">
          <div class="container section">
            <div class="has-text-right">
              <h1 class="title is-1">Iniciar sesión</h1> <br>
              <p class="title is-3">VestiVenta!</p>
            </div>
          </div>
        </div>
        <div class="hero-footer">
          <p class="has-text-centered">Image © Glenn Carstens-Peters via unsplash</p>
        </div>
      </section>  
    </div>
    <div class="column is-4">
      <section class="hero is-fullheight">
        <div class="hero-heading">
          <div class="section has-text-centered">
            <img src="http://bulma.io/images/bulma-logo.png" alt="Bulma logo" width="150px">
          </div>
        </div>
        <div class="hero-body">
          <div class="container">
            <div class="columns">
              <div class="column is-8 is-offset-2">
                <form id="form1" runat="server">
                <div class="login-form">
                  <p class="control has-icon has-icon-right">
                    <asp:TextBox ID="txtUser" runat="server" placeholder="Ingresa tu Usuario" CssClass="input email-input"></asp:TextBox>
                    <span class="icon user">
                      <i class="fa fa-user"></i>
                    </span>
                  </p>
                  <p class="control has-icon has-icon-right">
                    <asp:TextBox ID="txtPass" runat="server" placeholder="Ingresa tu Contraseña"  CssClass="input password-input" TextMode="Password"></asp:TextBox>
                    <span class="icon user">
                      <i class="fa fa-lock"></i>
                    </span>
                  </p>
                  <p class="control login">
                    <asp:Button ID="btnLogin" runat="server" CssClass="button is-success is-outlined is-large is-fullwidth" Text="Ingresar" OnClick="btnLogin_Click" />
                  </p>
                </div>
               </form>
              </div>
            </div>
          </div>
        </div>
      </section>  
    </div>
  </div>

</body>
</html>