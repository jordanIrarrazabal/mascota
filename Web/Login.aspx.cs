﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            if (txtUser.Text == string.Empty || txtPass.Text == string.Empty)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Complete los campos')", true);
            }
            else
            {
                ServiceReference1.Service1Client s = new ServiceReference1.Service1Client();
                string contraseña = s.ContraseñaUsuario(txtUser.Text);
                if (contraseña == txtPass.Text)
                {
                    Session["user"] = txtUser.Text;
                    Session["pass"] = contraseña;
                    Response.Redirect("Index.aspx");
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Usuario no encontrado')", true);
                }
            }
        }
    }
}