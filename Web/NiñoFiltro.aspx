﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="NiñoFiltro.aspx.cs" Inherits="Web.NiñoFiltro" %>
<%@ MasterType VirtualPath="~/Site1.Master" %> 
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <asp:GridView ID="GridView1" runat="server" ShowHeader="False" GridLines="None" AutoGenerateColumns = "False"
       Font-Names = "Arial" DataSourceID="SqlDataSource1" BorderStyle="None" Width="50%" HorizontalAlign="Center" DataKeyNames="Id">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%#String.Format("detalleProducto.aspx?IdProducto={0}", Eval("id")) %>' Text="Ver Detalle" ></asp:HyperLink>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:ImageField DataImageUrlField="imagen" ControlStyle-Width="300">
<ControlStyle Width="300px"></ControlStyle>
        </asp:ImageField>
    </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:vestiventaConnectionString %>" SelectCommand="SELECT [nombre], [Id], [imagen] FROM [producto] WHERE (([destino] = @destino) AND ([categoria] = @categoria))">
        <SelectParameters>
            <asp:Parameter DefaultValue="3" Name="destino" Type="Int32" />
            <asp:QueryStringParameter Name="categoria" QueryStringField="Categoria" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
