﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web
{
    public partial class NiñoFiltro : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] == null)
            {
                this.Master.LabelMensaje = string.Empty;
                this.Master.ButtonEnable = false;
                this.Master.ButtonEnableCarrito = false;
            }
            else
            {
                string usuario = Session["user"].ToString();
                this.Master.LabelMensaje = "Bienvenido: " + usuario;
                this.Master.ButtonEnable = true;
                this.Master.ButtonEnableCarrito = true;
            }
        }
    }
}