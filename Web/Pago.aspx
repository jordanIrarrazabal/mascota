﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Pago.aspx.cs" Inherits="Web.Pago" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
        <style>
    .bodyLogin{
	background: linear-gradient(#F2F2F2, #F2F2F2);
}
.bodyMenu{
	background: linear-gradient(#DBDBDB, #DBDBDB);
}
#contenedor{
	border-width:5px;	
    border-style:dashed;
    border-color: #D8D8D8;

	height: 70%;
	width: 40%;
	margin: auto;
  	position: absolute;
  	top: 0; left: 0; bottom: 0; right: 0;
}
#contenedorImagen{
	
	width: 50%;
	height: 30%;
	margin: auto;
  	position: absolute;
  	top: -55%; left: 0; bottom: 0; right: 0;
}
.imgLogin{
	width: 100%;
	height: 100%;
}
#contenedorTabla{
	
	height: 50%;
	width: 85%;
	margin: auto;
  	position: absolute;
  	top: 35%; left: 0; bottom: 0; right: 0;
}
#contenedorTabla2{
	
	height: 90%;
	width: 85%;
	margin: auto;
  	position: absolute;
  	top: 0; left: 0; bottom: 0; right: 0;
}
#tabla{
	width: 100%;
	height: 70%;

	margin: auto;
  	position: absolute;
  	top: 0; left: 0; bottom: 0; right: 0;
}
.inputLogin{
	width: 100%;
	height: 100%;
	border-radius: 5px;
}
.inputLoginButton{
	width: 100%;
	height: 100%;
	border:none;
	background-color: #4CAF50;
	color:white;
	border-radius: 5px;
}
.inputLoginButton:hover{
	box-shadow: 2px 2px 20px black;
}
        .auto-style1 {
            text-align: center;
        }
            .auto-style2 {
                font-family: verdana;
                font-weight: bold;
                font-size: 150%;
            }
    </style>
</head>
<body class="bodyLogin">
	<form id="form1" runat="server">
	<div id="contenedor">
		<div id="contenedorImagen">
			<img class="imgLogin" src="img/banco.jpg">
		</div>
		<div id="contenedorTabla">
			<table id="tabla">
				<tr>
					<td> <span class="auto-style2">Numero Cuenta</span> </td>
					<td> &nbsp;<asp:TextBox ID="txtNumero" runat="server" CssClass="inputLogin"></asp:TextBox>
                    </td>
				</tr>
				<tr>
					<td> <font face="verdana" style="font-weight:bold;font-size:150%;">Contraseña</font> </td>
					<td> &nbsp;<asp:TextBox ID="txtPass" runat="server" CssClass="inputLogin" TextMode="Password"></asp:TextBox>
                    </td>
				</tr>
				<tr>
					<td></td>
					<td class="auto-style1">  
                        <asp:Button ID="btnLogin" runat="server" CssClass="inputLoginButton" Text="Pagar" OnClick="btnLogin_Click" />
                    </td>
				</tr>
				<tr>
					<td></td>
					<td class="auto-style1">  
                        <asp:Button ID="Button2" runat="server" CssClass="inputLoginButton" PostBackUrl="~/Carrito.aspx" Text="Volver" />
                    </td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td class="auto-style1">  
                        &nbsp;</td>
				</tr>
			</table>
		</div>
	</div>
	</form>
</body>
</html>
