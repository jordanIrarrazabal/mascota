﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web
{
    public partial class Pago : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            ServiceReference1.Service1Client s = new ServiceReference1.Service1Client();
            WebServiceBanco.ServicioWebMySqlSoapClient b = new WebServiceBanco.ServicioWebMySqlSoapClient();
            string usuario = Session["user"].ToString();
            int num_cuenta = Convert.ToInt32(txtNumero.Text);


            if (txtPass.Text == b.ContraseñaUsuarioBanco(num_cuenta))
            {
                int total = s.TotalPagar(usuario);
                string retiro = Request.QueryString["Retiro"];
                string direccion = s.DireccionUsuario(usuario);

                int saldo = b.SaldoUsuarioBanco(num_cuenta);
                int nuevoSaldo = saldo - total;

                if (saldo > total)
                {
                    if (retiro == "Despacho")
                    {
                        s.ActualizarPedidosRetiro(usuario, retiro);
                        s.ActualizarPedidosDireccion(usuario, direccion);
                    }
                    else if (retiro == "Tienda")
                    {
                        s.ActualizarPedidosRetiro(usuario, retiro);
                    }

                    //ACTUALIZAR STOCK
                    int cant = s.CountTotalProductoCarritoCompraPagado(usuario);

                    int idProductoCarrito = s.IdProductoCarritoCompraPagado(usuario);

                    int cantidadProductoCarrito = s.CantidadProductoCarritoCompraPagado(idProductoCarrito, usuario);

                    int stockPrevio = s.StockProducto(idProductoCarrito);

                    int nuevoStock = stockPrevio - cantidadProductoCarrito;

                    s.ActualizarCantidadProducto(idProductoCarrito, nuevoStock);

                    //END
                    b.ActualizarSaldo(nuevoSaldo, num_cuenta);
                    s.ActualizarPedidos(usuario);


                    b.InsertarPagos(num_cuenta, total);

                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Pedido Realizado Correctamente')", true);

                    Response.AddHeader("REFRESH", "2;URL=Carrito.aspx");
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Saldo Insuficiente :(')", true);
                    Response.AddHeader("REFRESH", "2;URL=Carrito.aspx");
                }
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Usuario Incorrecto')", true);
            }
         }
     }
}
