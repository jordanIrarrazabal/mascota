﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Registrar.aspx.cs" Inherits="Web.Registrar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
 <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Registro!</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="https://dansup.github.io/bulma-templates/css/bulma.css">
  <link rel="stylesheet" type="text/css" href="https://dansup.github.io/bulma-templates/css/base.css">
</head>
<body>
  <section class="hero is-fullheight is-dark is-bold">
    <div class="hero-body">
      <div class="container">
        <div class="columns is-vcentered">
          <div class="column is-4 is-offset-4">
            <h1 class="title">
              Registrate!
            </h1>
            <form id="form1" runat="server">
            <div class="box">
              <label class="label">Usuario</label>
              <p class="control">
                <asp:TextBox ID="txtUser" runat="server" CssClass="input"></asp:TextBox>
              </p>
              <label class="label">Contraseña</label>
              <p class="control">
                <asp:TextBox ID="txtPass" runat="server" CssClass="input" TextMode="Password"></asp:TextBox>
              </p>
              <label class="label">Confirmar Contraseña</label>
              <p class="control">
                <asp:TextBox ID="txtPassConfirmar" runat="server" CssClass="input" TextMode="Password"></asp:TextBox>
              </p>
              <hr>
              <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtPassConfirmar" ControlToValidate="txtPass" Display="Dynamic" ErrorMessage="Contraseña no coinciden" ForeColor="#FF3300" ValidationGroup="Registrar"></asp:CompareValidator>
              <label class="label">Nombre</label>
              <p class="control">
                <asp:TextBox ID="txtNombre" runat="server" CssClass="input"></asp:TextBox>
              </p>
              <label class="label">Apellido</label>
              <p class="control">
                <asp:TextBox ID="txtApellido" runat="server" CssClass="input"></asp:TextBox>
              </p>
              <label class="label">Email</label>
              <p class="control">
              <asp:TextBox ID="txtEmail" runat="server" CssClass="input"></asp:TextBox>
              </p>
              <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail" Display="Dynamic" ErrorMessage="Email no válido" ForeColor="#FF3300" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="Registrar"></asp:RegularExpressionValidator>
               <label class="label">Fono</label>
              <p class="control">
               <asp:TextBox ID="txtFono" runat="server" CssClass="input" TextMode="Number"></asp:TextBox>
              </p>
               <label class="label">Direccion</label>
              <p class="control">
               <asp:TextBox ID="txtDireccion" runat="server" CssClass="input"></asp:TextBox>
              </p>
              <hr>
              <p class="control">
                <asp:Button ID="btnRegistrar" runat="server" CssClass="button is-primary" Text="Registrarse" OnClick="btnRegistrar_Click" ValidationGroup="Registrar" />
              </p>
               </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</body>
</html>
