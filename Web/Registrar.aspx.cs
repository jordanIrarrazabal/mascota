﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web
{
    public partial class Registrar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnRegistrar_Click(object sender, EventArgs e)
        {
            if (txtUser.Text == string.Empty || txtPassConfirmar.Text == string.Empty || txtNombre.Text == string.Empty || txtApellido.Text == string.Empty || txtEmail.Text == string.Empty || txtFono.Text == string.Empty || txtDireccion.Text == string.Empty)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Complete los campos')", true);
            }else
            {
                ServiceReference1.Service1Client s = new ServiceReference1.Service1Client();
                string p = s.ContraseñaUsuario(txtUser.Text);

                if (p == null)
                {
                    int fono = Convert.ToInt32(txtFono.Text);
                    s.CrearUsuario(txtUser.Text, txtPass.Text, txtNombre.Text, txtApellido.Text, txtEmail.Text, fono, txtDireccion.Text);
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Usuario Registrado')", true);
                    Response.AddHeader("REFRESH", "2;URL=Inicio.aspx");
                }else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('El nombre de usuario ya existe')", true);
                }

            }
        }
    }
}