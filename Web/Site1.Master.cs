﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web
{
    public partial class Site1 : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }



        public string LabelMensaje
        {
            get
            {
                return lblWelcome.Text;
            }
            set
            {
                lblWelcome.Text = value;
            }
        }


        public bool ButtonEnable
        {
            get
            {
                return btnLogout.Visible;
            }
            set
            {
                btnLogout.Visible = value;
            }
        }

        public string ButtonText
        {
            get
            {
                return btnLogout.Text;
            }
            set
            {
                btnLogout.Text = value;
            }
        }

        public bool ButtonEnableCarrito
        {
            get
            {
                return btnCarrito.Visible;
            }
            set
            {
                btnCarrito.Visible = value;
            }
        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            Session["user"] = null;
            Session["pass"] = null;
            Response.Redirect("Inicio.aspx");
        }

        protected void btnCarrito_Click(object sender, EventArgs e)
        {
            Response.Redirect("Carrito.aspx");
        }
    }
}