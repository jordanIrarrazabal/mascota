﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="detalleProducto.aspx.cs" Inherits="Web.detalleProducto" %>
<%@ MasterType VirtualPath="~/Site1.Master" %> 
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style4 {
            height: 23px;
        }
        .auto-style5 {
            font-size: x-large;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="ContentPlaceHolder2">
    <table style="width:100%;">
        <tr>
            <td class="auto-style3" rowspan="6">
                <asp:Image ID="imagenProducto" runat="server" />
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><span class="auto-style5"><strong>Nombre:</strong></span>
                <asp:Label ID="lblNombre" runat="server" Text="Label"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style4"><strong><span class="auto-style5">Precio:</span></strong>
                <asp:Label ID="lblPrecio" runat="server" Text="Label"></asp:Label>
            </td>
        </tr>
        <tr>
            <td><span class="auto-style5"><strong>Stock: </strong></span>
                <asp:Label ID="lblStock" runat="server" Text="Label"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style5"><strong>Cantidad Agregar:
                <asp:TextBox ID="txtCantidad" runat="server" Width="25px"  onkeydown = "return (!(event.keyCode>=65) && event.keyCode!=32)"></asp:TextBox>
                </strong>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnCarro" runat="server" BackColor="White" Font-Names="Verdana" OnClick="Button1_Click" Text="Agregar al carrito" ValidationGroup="Add" />
            </td>
        </tr>
    </table>
</asp:Content>

