﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Web
{
    public partial class detalleProducto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["user"] == null)
            {
                this.Master.LabelMensaje = string.Empty;
                this.Master.ButtonEnable = false;
                this.Master.ButtonEnableCarrito = false;
            }
            else
            {
                string usuario = Session["user"].ToString();
                this.Master.LabelMensaje = "Bienvenido: " + usuario;
                this.Master.ButtonEnable = true;
                this.Master.ButtonEnableCarrito = true;
            }

            int parametro = Convert.ToInt32(Request.QueryString["idProducto"]);
            imagenProducto.ImageUrl = "~/img/producto"+ parametro + ".jpg";
            imagenProducto.Width = 250;
            ServiceReference1.Service1Client s = new ServiceReference1.Service1Client();
            lblNombre.Text = s.NombreProducto(parametro);
            lblPrecio.Text = s.PrecioProducto(parametro).ToString();
            lblStock.Text = s.StockProducto(parametro).ToString();

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (Session["user"] == null)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Inicie Sesión')", true);
            }
            else
            {
                if (txtCantidad.Text == string.Empty || txtCantidad.Text == "0")
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Ingrese un valor en el campo CANTIDAD')", true);
                }
                else
                {
                    ServiceReference1.Service1Client s = new ServiceReference1.Service1Client();
                    int idP = Convert.ToInt32(Request.QueryString["idProducto"]);
                    int stockProducto = s.StockProducto(idP);
                    int cantidadIngresada = Convert.ToInt32(txtCantidad.Text);

                    if (cantidadIngresada > stockProducto)
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('No hay suficiente STOCK')", true);
                    }
                    else
                    {
                        int idProducto = Convert.ToInt32(Request.QueryString["idProducto"]);
                        int precio = s.PrecioProducto(idProducto);
                        int cantidad = Convert.ToInt32(txtCantidad.Text);
                        int total = precio * cantidad;
                        string usuario = Session["user"].ToString();

                        s.InsertarPedido(idProducto, usuario, cantidad, total, 0);
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Producto agregado al carrito')", true);
                    }
                }
            }
        }
    }
}