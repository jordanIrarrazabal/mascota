﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WebService
{
    public class Conector
    {
        private String cadena;
        private String sql;

        public String Sql
        {
            get { return sql; }
            set { sql = value; }
        }

        public Conector()
        {                           //(local)\sqlexpress
            cadena = @"Data Source=(LocalDB)\MSSQLLocalDB;" +
                     @"AttachDbFilename='|DataDirectory|\vestiventa.mdf';" +
                    "Integrated Security=true;";
        }

        public String NombreProducto()
        {
            String Nombre = "";
            try
            {
                SqlConnection con = new SqlConnection(cadena);
                SqlCommand com = new SqlCommand(Sql, con);
                con.Open();
                SqlDataReader dr = com.ExecuteReader(CommandBehavior.CloseConnection);
                dr.Read();
                Nombre = dr.GetString(dr.GetOrdinal("N"));
                con.Close();
            }
            catch (Exception e)
            {
                Nombre = null;
            }
            return Nombre;

        }

        public int CountProducto()
        {
            int Precio;
            try
            {
                SqlConnection con = new SqlConnection(cadena);
                SqlCommand com = new SqlCommand(Sql, con);
                con.Open();
                SqlDataReader dr = com.ExecuteReader(CommandBehavior.CloseConnection);
                dr.Read();
                Precio = dr.GetInt32(dr.GetOrdinal("P"));
                con.Close();
            }
            catch (Exception e)
            {
                Precio = 0;
            }
            return Precio;

        }

        public int PrecioProducto()
        {
            int Precio;
            try
            {
                SqlConnection con = new SqlConnection(cadena);
                SqlCommand com = new SqlCommand(Sql, con);
                con.Open();
                SqlDataReader dr = com.ExecuteReader(CommandBehavior.CloseConnection);
                dr.Read();
                Precio = dr.GetInt32(dr.GetOrdinal("P"));
                con.Close();
            }
            catch (Exception e)
            {
                Precio = 0;
            }
            return Precio;

        }

        public int StockProducto()
        {
            int Stock;
            try
            {
                SqlConnection con = new SqlConnection(cadena);
                SqlCommand com = new SqlCommand(Sql, con);
                con.Open();
                SqlDataReader dr = com.ExecuteReader(CommandBehavior.CloseConnection);
                dr.Read();
                Stock = dr.GetInt32(dr.GetOrdinal("S"));
                con.Close();
            }
            catch (Exception e)
            {
                Stock = 0;
            }
            return Stock;

        }


        public String ContraseñaUsuario()
        {
            String Contraseña = "";
            try
            {
                SqlConnection con = new SqlConnection(cadena);
                SqlCommand com = new SqlCommand(Sql, con);
                con.Open();
                SqlDataReader dr = com.ExecuteReader(CommandBehavior.CloseConnection);
                dr.Read();
                Contraseña = dr.GetString(dr.GetOrdinal("C"));
                con.Close();
            }
            catch (Exception e)
            {
                Contraseña = null;
            }
            return Contraseña;

        }

        public int InsertarPedido()
        {
            int resultado = 0;
            try
            {
                SqlConnection con = new SqlConnection(cadena);
                SqlCommand com = new SqlCommand(Sql, con);
                con.Open();
                resultado = com.ExecuteNonQuery(); // 0 o nro + éxito
                con.Close();
            }
            catch (Exception e)
            {
                resultado = -1;
            }
            return resultado;
        }

        public DataSet ListarPedidoUsuario()
        {
            SqlConnection con = new SqlConnection(cadena);
            SqlDataAdapter ad = new SqlDataAdapter(Sql, con);
            DataSet ds = new DataSet();
            try
            {
                ad.Fill(ds);
            }
            catch (Exception ex)
            {
                ds = null;
            }

            con.Close();
            return ds;
        }


        public int EliminarPedido()
        {
            int resultado = 0;
            try
            {
                SqlConnection con = new SqlConnection(cadena);
                SqlCommand com = new SqlCommand(Sql, con);
                con.Open();
                resultado = com.ExecuteNonQuery(); // 0 o nro + éxito
                con.Close();
            }
            catch (Exception e)
            {
                resultado = -1;
            }
            return resultado;
        }

        public String DireccionUsuario()
        {
            String Contraseña = "";
            try
            {
                SqlConnection con = new SqlConnection(cadena);
                SqlCommand com = new SqlCommand(Sql, con);
                con.Open();
                SqlDataReader dr = com.ExecuteReader(CommandBehavior.CloseConnection);
                dr.Read();
                Contraseña = dr.GetString(dr.GetOrdinal("D"));
                con.Close();
            }
            catch (Exception e)
            {
                Contraseña = null;
            }
            return Contraseña;

        }


        public int TotalPagar()
        {
            int Stock;
            try
            {
                SqlConnection con = new SqlConnection(cadena);
                SqlCommand com = new SqlCommand(Sql, con);
                con.Open();
                SqlDataReader dr = com.ExecuteReader(CommandBehavior.CloseConnection);
                dr.Read();
                Stock = dr.GetInt32(dr.GetOrdinal("S"));
                con.Close();
            }
            catch (Exception e)
            {
                Stock = 0;
            }
            return Stock;

        }

        //UPDATE STOCK

            //TOTAL PRODUCTOS A PAGAR POR USUARIO
        public int CountTotalProductoCarritoCompraPagado(string Usuario)
        {
            int Stock;
            try
            {
                SqlConnection con = new SqlConnection(cadena);
                SqlCommand com = new SqlCommand(Sql, con);
                con.Open();
                SqlDataReader dr = com.ExecuteReader(CommandBehavior.CloseConnection);
                dr.Read();
                Stock = dr.GetInt32(dr.GetOrdinal("S"));
                con.Close();
            }
            catch (Exception e)
            {
                Stock = 0;
            }
            return Stock;

        }

        //ID DE PRODUCTO A PAGAR POR USUARIO
        public int IdProductoCarritoCompraPagado(string Usuario)
        {
            int Stock;
            try
            {
                SqlConnection con = new SqlConnection(cadena);
                SqlCommand com = new SqlCommand(Sql, con);
                con.Open();
                SqlDataReader dr = com.ExecuteReader(CommandBehavior.CloseConnection);
                dr.Read();
                Stock = dr.GetInt32(dr.GetOrdinal("S"));
                con.Close();
            }
            catch (Exception e)
            {
                Stock = 0;
            }
            return Stock;

        }

        //CANTIDAD QUE EL USUARIO VA A COMPRAR DEL PRODUCTO, POR ID
        public int CantidadProductoCarritoCompraPagado(int idProducto, string Usuario)
        {
            int Stock;
            try
            {
                SqlConnection con = new SqlConnection(cadena);
                SqlCommand com = new SqlCommand(Sql, con);
                con.Open();
                SqlDataReader dr = com.ExecuteReader(CommandBehavior.CloseConnection);
                dr.Read();
                Stock = dr.GetInt32(dr.GetOrdinal("S"));
                con.Close();
            }
            catch (Exception e)
            {
                Stock = 0;
            }
            return Stock;

        }

        //ACTUALIZAR STOCK FINAL CON EL VALIR NUEVO
        public int ActualizarCantidadProducto(int id, int stock)
        {
            int Stock;
            try
            {
                SqlConnection con = new SqlConnection(cadena);
                SqlCommand com = new SqlCommand(Sql, con);
                con.Open();
                SqlDataReader dr = com.ExecuteReader(CommandBehavior.CloseConnection);
                dr.Read();
                Stock = dr.GetInt32(dr.GetOrdinal("S"));
                con.Close();
            }
            catch (Exception e)
            {
                Stock = 0;
            }
            return Stock;

        }

        //END

        public int ActualizarPedidos()
        {
            int resultado = 0;
            try
            {
                SqlConnection con = new SqlConnection(cadena);
                SqlCommand com = new SqlCommand(Sql, con);
                con.Open();
                resultado = com.ExecuteNonQuery(); // 0 o nro + éxito
                con.Close();
            }
            catch (Exception e)
            {
                resultado = -1;
            }
            return resultado;
        }

        public int ActualizarPedidosRetiro()
        {
            int resultado = 0;
            try
            {
                SqlConnection con = new SqlConnection(cadena);
                SqlCommand com = new SqlCommand(Sql, con);
                con.Open();
                resultado = com.ExecuteNonQuery(); // 0 o nro + éxito
                con.Close();
            }
            catch (Exception e)
            {
                resultado = -1;
            }
            return resultado;
        }

        public int ActualizarPedidosDireccion()
        {
            int resultado = 0;
            try
            {
                SqlConnection con = new SqlConnection(cadena);
                SqlCommand com = new SqlCommand(Sql, con);
                con.Open();
                resultado = com.ExecuteNonQuery(); // 0 o nro + éxito
                con.Close();
            }
            catch (Exception e)
            {
                resultado = -1;
            }
            return resultado;
        }

        public int CrearUsuario()
        {
            int resultado = 0;
            try
            {
                SqlConnection con = new SqlConnection(cadena);
                SqlCommand com = new SqlCommand(Sql, con);
                con.Open();
                resultado = com.ExecuteNonQuery(); // 0 o nro + éxito
                con.Close();
            }
            catch (Exception e)
            {
                resultado = -1;
            }
            return resultado;
        }


        //SERVICIOS WEB DESKTOP NETBEANS

        public String ContraseñaAdmin()
        {
            String Contraseña = "";
            try
            {
                SqlConnection con = new SqlConnection(cadena);
                SqlCommand com = new SqlCommand(Sql, con);
                con.Open();
                SqlDataReader dr = com.ExecuteReader(CommandBehavior.CloseConnection);
                dr.Read();
                Contraseña = dr.GetString(dr.GetOrdinal("C"));
                con.Close();
            }
            catch (Exception e)
            {
                Contraseña = null;
            }
            return Contraseña;

        }

        public DataSet ListaProductos()
        {
            SqlConnection con = new SqlConnection(cadena);
            SqlDataAdapter ad = new SqlDataAdapter(Sql, con);
            DataSet ds = new DataSet();
            try
            {
                ad.Fill(ds);
            }
            catch (Exception ex)
            {
                ds = null;
            }

            con.Close();
            return ds;
        }

        public int CountCategoria()
        {
            int Stock;
            try
            {
                SqlConnection con = new SqlConnection(cadena);
                SqlCommand com = new SqlCommand(Sql, con);
                con.Open();
                SqlDataReader dr = com.ExecuteReader(CommandBehavior.CloseConnection);
                dr.Read();
                Stock = dr.GetInt32(dr.GetOrdinal("S"));
                con.Close();
            }
            catch (Exception e)
            {
                Stock = 0;
            }
            return Stock;

        }

        public String NombreCategoria()
        {
            String Nombre = "";
            try
            {
                SqlConnection con = new SqlConnection(cadena);
                SqlCommand com = new SqlCommand(Sql, con);
                con.Open();
                SqlDataReader dr = com.ExecuteReader(CommandBehavior.CloseConnection);
                dr.Read();
                Nombre = dr.GetString(dr.GetOrdinal("N"));
                con.Close();
            }
            catch (Exception e)
            {
                Nombre = null;
            }
            return Nombre;

        }


        public String NombreCategoriaPorId()
        {
            String Nombre = "";
            try
            {
                SqlConnection con = new SqlConnection(cadena);
                SqlCommand com = new SqlCommand(Sql, con);
                con.Open();
                SqlDataReader dr = com.ExecuteReader(CommandBehavior.CloseConnection);
                dr.Read();
                Nombre = dr.GetString(dr.GetOrdinal("N"));
                con.Close();
            }
            catch (Exception e)
            {
                Nombre = null;
            }
            return Nombre;

        }

        //DESTINO

        public String NombreDestino()
        {
            String Nombre = "";
            try
            {
                SqlConnection con = new SqlConnection(cadena);
                SqlCommand com = new SqlCommand(Sql, con);
                con.Open();
                SqlDataReader dr = com.ExecuteReader(CommandBehavior.CloseConnection);
                dr.Read();
                Nombre = dr.GetString(dr.GetOrdinal("N"));
                con.Close();
            }
            catch (Exception e)
            {
                Nombre = null;
            }
            return Nombre;

        }

        public int CountDestino()
        {
            int Stock;
            try
            {
                SqlConnection con = new SqlConnection(cadena);
                SqlCommand com = new SqlCommand(Sql, con);
                con.Open();
                SqlDataReader dr = com.ExecuteReader(CommandBehavior.CloseConnection);
                dr.Read();
                Stock = dr.GetInt32(dr.GetOrdinal("S"));
                con.Close();
            }
            catch (Exception e)
            {
                Stock = 0;
            }
            return Stock;

        }


        public String NombreDestinoPorId()
        {
            String Nombre = "";
            try
            {
                SqlConnection con = new SqlConnection(cadena);
                SqlCommand com = new SqlCommand(Sql, con);
                con.Open();
                SqlDataReader dr = com.ExecuteReader(CommandBehavior.CloseConnection);
                dr.Read();
                Nombre = dr.GetString(dr.GetOrdinal("N"));
                con.Close();
            }
            catch (Exception e)
            {
                Nombre = null;
            }
            return Nombre;

        }

        public int PrecioProductoPorNombre()
        {
            int Precio;
            try
            {
                SqlConnection con = new SqlConnection(cadena);
                SqlCommand com = new SqlCommand(Sql, con);
                con.Open();
                SqlDataReader dr = com.ExecuteReader(CommandBehavior.CloseConnection);
                dr.Read();
                Precio = dr.GetInt32(dr.GetOrdinal("P"));
                con.Close();
            }
            catch (Exception e)
            {
                Precio = 0;
            }
            return Precio;

        }

        public int StockProductoPorNombre()
        {
            int Stock;
            try
            {
                SqlConnection con = new SqlConnection(cadena);
                SqlCommand com = new SqlCommand(Sql, con);
                con.Open();
                SqlDataReader dr = com.ExecuteReader(CommandBehavior.CloseConnection);
                dr.Read();
                Stock = dr.GetInt32(dr.GetOrdinal("S"));
                con.Close();
            }
            catch (Exception e)
            {
                Stock = 0;
            }
            return Stock;

        }

        public String ImagenProductoPorNombre()
        {
            String Nombre = "";
            try
            {
                SqlConnection con = new SqlConnection(cadena);
                SqlCommand com = new SqlCommand(Sql, con);
                con.Open();
                SqlDataReader dr = com.ExecuteReader(CommandBehavior.CloseConnection);
                dr.Read();
                Nombre = dr.GetString(dr.GetOrdinal("N"));
                con.Close();
            }
            catch (Exception e)
            {
                Nombre = null;
            }
            return Nombre;

        }
        public int ActualizarProducto()
        {
            int resultado = 0;
            try
            {
                SqlConnection con = new SqlConnection(cadena);
                SqlCommand com = new SqlCommand(Sql, con);
                con.Open();
                resultado = com.ExecuteNonQuery(); // 0 o nro + éxito
                con.Close();
            }
            catch (Exception e)
            {
                resultado = -1;
            }
            return resultado;
        }

        public int IdCategoriaPorNombre()
        {
            int Precio;
            try
            {
                SqlConnection con = new SqlConnection(cadena);
                SqlCommand com = new SqlCommand(Sql, con);
                con.Open();
                SqlDataReader dr = com.ExecuteReader(CommandBehavior.CloseConnection);
                dr.Read();
                Precio = dr.GetInt32(dr.GetOrdinal("P"));
                con.Close();
            }
            catch (Exception e)
            {
                Precio = 0;
            }
            return Precio;

        }

        public int IdDestinoPorNombre()
        {
            int Precio;
            try
            {
                SqlConnection con = new SqlConnection(cadena);
                SqlCommand com = new SqlCommand(Sql, con);
                con.Open();
                SqlDataReader dr = com.ExecuteReader(CommandBehavior.CloseConnection);
                dr.Read();
                Precio = dr.GetInt32(dr.GetOrdinal("P"));
                con.Close();
            }
            catch (Exception e)
            {
                Precio = 0;
            }
            return Precio;

        }

        public int EliminarProducto()
        {
            int resultado = 0;
            try
            {
                SqlConnection con = new SqlConnection(cadena);
                SqlCommand com = new SqlCommand(Sql, con);
                con.Open();
                resultado = com.ExecuteNonQuery(); // 0 o nro + éxito
                con.Close();
            }
            catch (Exception e)
            {
                resultado = -1;
            }
            return resultado;
        }

        public int UltimoProducto()
        {
            int Precio;
            try
            {
                SqlConnection con = new SqlConnection(cadena);
                SqlCommand com = new SqlCommand(Sql, con);
                con.Open();
                SqlDataReader dr = com.ExecuteReader(CommandBehavior.CloseConnection);
                dr.Read();
                Precio = dr.GetInt32(dr.GetOrdinal("P"));
                con.Close();
            }
            catch (Exception e)
            {
                Precio = 0;
            }
            return Precio;

        }

        public int InsertarProducto()
        {
            int resultado = 0;
            try
            {
                SqlConnection con = new SqlConnection(cadena);
                SqlCommand com = new SqlCommand(Sql, con);
                con.Open();
                resultado = com.ExecuteNonQuery(); // 0 o nro + éxito
                con.Close();
            }
            catch (Exception e)
            {
                resultado = -1;
            }
            return resultado;
        }

        public int InsertarCategoria()
        {
            int resultado = 0;
            try
            {
                SqlConnection con = new SqlConnection(cadena);
                SqlCommand com = new SqlCommand(Sql, con);
                con.Open();
                resultado = com.ExecuteNonQuery(); // 0 o nro + éxito
                con.Close();
            }
            catch (Exception e)
            {
                resultado = -1;
            }
            return resultado;
        }

        public int EliminarCategoria()
        {
            int resultado = 0;
            try
            {
                SqlConnection con = new SqlConnection(cadena);
                SqlCommand com = new SqlCommand(Sql, con);
                con.Open();
                resultado = com.ExecuteNonQuery(); // 0 o nro + éxito
                con.Close();
            }
            catch (Exception e)
            {
                resultado = -1;
            }
            return resultado;
        }

        //LISTA PEDIDOS

        public DataSet ListaPedido()
        {
            SqlConnection con = new SqlConnection(cadena);
            SqlDataAdapter ad = new SqlDataAdapter(Sql, con);
            DataSet ds = new DataSet();
            try
            {
                ad.Fill(ds);
            }
            catch (Exception ex)
            {
                ds = null;
            }

            con.Close();
            return ds;
        }

        public int CountPedido()
        {
            int Stock;
            try
            {
                SqlConnection con = new SqlConnection(cadena);
                SqlCommand com = new SqlCommand(Sql, con);
                con.Open();
                SqlDataReader dr = com.ExecuteReader(CommandBehavior.CloseConnection);
                dr.Read();
                Stock = dr.GetInt32(dr.GetOrdinal("S"));
                con.Close();
            }
            catch (Exception e)
            {
                Stock = 0;
            }
            return Stock;

        }

        //LISTA USUARIOS

        public DataSet ListaUsuarios()
        {
            SqlConnection con = new SqlConnection(cadena);
            SqlDataAdapter ad = new SqlDataAdapter(Sql, con);
            DataSet ds = new DataSet();
            try
            {
                ad.Fill(ds);
            }
            catch (Exception ex)
            {
                ds = null;
            }

            con.Close();
            return ds;
        }

        public int CountUsuario()
        {
            int Stock;
            try
            {
                SqlConnection con = new SqlConnection(cadena);
                SqlCommand com = new SqlCommand(Sql, con);
                con.Open();
                SqlDataReader dr = com.ExecuteReader(CommandBehavior.CloseConnection);
                dr.Read();
                Stock = dr.GetInt32(dr.GetOrdinal("S"));
                con.Close();
            }
            catch (Exception e)
            {
                Stock = 0;
            }
            return Stock;

        }
    }
}