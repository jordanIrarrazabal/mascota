﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WebService
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IService1" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IService1
    {

        [OperationContract]
        String NombreProducto(int id);

        [OperationContract]
        int CountProducto();

        [OperationContract]
        int PrecioProducto(int id);

        [OperationContract]
        int StockProducto(int id);

        [OperationContract]
        String ContraseñaUsuario(string usuario);

        [OperationContract]
        int InsertarPedido(int IdProducto, string Usuario, int Cantidad, int Total, int Estado);

        [OperationContract]
        List<Pedido> ListarPedidoUsuario(string usuario);

        [OperationContract]
        int EliminarPedido(int Id, string usuario);

        [OperationContract]
        string DireccionUsuario(string usuario);

        [OperationContract]
        int TotalPagar(string Usuario);

        [OperationContract]
        int CountTotalProductoCarritoCompraPagado(string Usuario);

        [OperationContract]
        int IdProductoCarritoCompraPagado(string Usuario);

        [OperationContract]
        int CantidadProductoCarritoCompraPagado(int idProducto, string Usuario);

        [OperationContract]
        int ActualizarCantidadProducto(int id, int stock);

        [OperationContract]
        int ActualizarPedidos(string usuario);

        [OperationContract]
        int ActualizarPedidosRetiro(string usuario, string retiro);

        [OperationContract]
        int ActualizarPedidosDireccion(string usuario, string direccion);

        [OperationContract]
        int CrearUsuario(string Usuario, string contraseña, string nombre, string apellido, string email, int fono, string direccion);

        //SERVICIOS WEB DESKTOP NETBEANS

        [OperationContract]
        String ContraseñaAdmin(string usuario);

        [OperationContract]
        List<producto> ListaProductos();

        [OperationContract]
        int CountCategoria();

        [OperationContract]
        String NombreCategoria(string nombreProducto);

        [OperationContract]
        String NombreCategoriaPorId(int id);

        [OperationContract]
        String NombreDestino(string nombreProducto);

        [OperationContract]
        int CountDestino();

        [OperationContract]
        String NombreDestinoPorId(int id);

        [OperationContract]
        int PrecioProductoPorNombre(string nombre);

        [OperationContract]
        int StockProductoPorNombre(string nombre);

        [OperationContract]
        String ImagenProductoPorNombre(string nombre);

        [OperationContract]
        int ActualizarProducto(string nombreI, string nombreF, int categoria, int destino, int precio, int stock, string imagen);

        [OperationContract]
        int IdCategoriaPorNombre(string nombre);

        [OperationContract]
        int IdDestinoPorNombre(string nombre);

        [OperationContract]
        int EliminarProducto(string nombre);

        [OperationContract]
        int UltimoProducto();

        [OperationContract]
        int InsertarProducto(int id, string nombre, int categoria, int destino, int precio, int stock, string imagen);

        [OperationContract]
        int InsertarCategoria(int id, string nombre);

        [OperationContract]
        int EliminarCategoria(string nombre);

        //LISTADO PEDIDO
        [OperationContract]
        List<Pedido> ListaPedido();

        [OperationContract]
        int CountPedido();

        //LISTA USUARIOS
        [OperationContract]
        List<usuario> ListaUsuarios();

        [OperationContract]
        int CountUsuario();
    }
}
