﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Web;

namespace WebService
{
    [DataContract]
    public class Pedido
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int IdProducto { get; set; }
        [DataMember]
        public String Usuario { get; set; }
        [DataMember]
        public int Cantidad { get; set; }
        [DataMember]
        public int Total { get; set; }
        [DataMember]
        public Boolean Estado { get; set; }
        [DataMember]
        public String Retiro { get; set; }
        [DataMember]
        public String Direccion { get; set; }

    }
}