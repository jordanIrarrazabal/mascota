﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WebService
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Service1" en el código, en svc y en el archivo de configuración.
    // NOTE: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Service1.svc o Service1.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class Service1 : IService1
    {
        public string NombreProducto(int id)
        {
            if (id < 1)
                return "";
            String nombre = "";
            Conector c = new Conector();
            c.Sql = "SELECT NOMBRE as N FROM producto " +
                    " WHERE ID = " + id;
            nombre = c.NombreProducto();
            return nombre;
        }

        public int CountProducto()
        {
            int cant;
            Conector c = new Conector();
            c.Sql = "SELECT count(Id) as P FROM producto ";
            cant = c.CountProducto();
            return cant;
        }

        public int PrecioProducto(int id)
        {
            if (id < 1)
                return 0;
            int precio;
            Conector c = new Conector();
            c.Sql = "SELECT PRECIO as P FROM producto " +
                    " WHERE ID = " + id;
            precio = c.PrecioProducto();
            return precio;
        }

        public int StockProducto(int id)
        {
            if (id < 1)
                return 0;
            int stock;
            Conector c = new Conector();
            c.Sql = "SELECT STOCK as S FROM producto " +
                    " WHERE ID = " + id;
            stock = c.StockProducto();
            return stock;
        }

        public string ContraseñaUsuario(string usuario)
        {
            if (usuario == string.Empty)
                return "";
            String contraseña = "";
            Conector c = new Conector();
            c.Sql = "SELECT contraseña as C FROM usuario " +
                    " WHERE nombre_usuario = '"+ usuario+"'";
            contraseña = c.ContraseñaUsuario();
            return contraseña;
        }

        public int InsertarPedido(int IdProducto, string Usuario, int Cantidad, int Total, int Estado)
        {
            int resultado = -1;
            Conector con = new Conector();
            con.Sql = "INSERT INTO PEDIDO VALUES (" +IdProducto + ",'" + Usuario + "',"+Cantidad+","+Total+","+Estado+",null,null)";
            resultado = con.InsertarPedido();
            return resultado;
        }

        public List<Pedido> ListarPedidoUsuario(string usuario)
        {
            List<Pedido> pedidos = new List<Pedido>();
            Pedido pedido;
            Conector con = new Conector();
            con.Sql = "Select Id, IdProducto, Usuario, Cantidad, Total, Estado from pedido where Usuario='"+usuario+"' and Estado='False'";
            DataSet ds = con.ListarPedidoUsuario();
            foreach (DataRow fila in ds.Tables[0].Rows)
            {
                pedido = new Pedido();
                pedido.Id = Int32.Parse(fila["Id"].ToString());
                pedido.IdProducto = Int32.Parse(fila["IdProducto"].ToString());
                pedido.Usuario = fila["Usuario"].ToString();
                pedido.Cantidad = Int32.Parse(fila["Cantidad"].ToString());
                pedido.Total = Int32.Parse(fila["Total"].ToString());
                pedido.Estado = Convert.ToBoolean(fila["Estado"].ToString());
                pedidos.Add(pedido);
            }
            return pedidos;
        }

        public int EliminarPedido(int Id, string usuario)
        {
            int resultado = -1;
            Conector con = new Conector();
            con.Sql = "DELETE FROM PEDIDO WHERE Estado=0 and Usuario='"+usuario+"' and Id="+Id;
            resultado = con.EliminarPedido();
            return resultado;
        }

        public string DireccionUsuario(string usuario)
        {
            if (usuario == string.Empty)
                return "";
            String direccion = "";
            Conector c = new Conector();
            c.Sql = "SELECT direccion as D FROM usuario " +
                    " WHERE nombre_usuario = '" + usuario + "'";
            direccion = c.DireccionUsuario();
            return direccion;
        }

        public int TotalPagar(string Usuario)
        {
            int total;
            Conector c = new Conector();
            c.Sql = "SELECT sum(Total) as S FROM pedido " +
                    " WHERE Usuario = '"+Usuario+"' and Estado=0";
            total = c.TotalPagar();
            return total;
        }

        //ACTUALIZAR STOCK

        //COUNT PARA OBTENER EL NUMERO DE PRODUCTOS A PAGAR POR USUARIO
        public int CountTotalProductoCarritoCompraPagado(string Usuario)
        {
            int total;
            Conector c = new Conector();
            c.Sql = "SELECT count(IdProducto) as S from pedido where Usuario='" + Usuario + "' and Estado='False'";
            total = c.TotalPagar();
            return total;
        }

        //OBTENER ID DEL LOS PRODUCTOS A PAGAR EN EL CARRITO
        public int IdProductoCarritoCompraPagado(string Usuario)
        {
            int total;
            Conector c = new Conector();
            c.Sql = "SELECT IdProducto as S from pedido where Usuario='" + Usuario + "' and Estado='False'";
            total = c.TotalPagar();
            return total;
        }
        //OBTENER LA CANTIDAD DE LOS PRODUCTOS A PAGAR EN EL CARRITO; EN BASE A LA ID DEL PRODUCTO SACADA ANTERIORMENTE

        public int CantidadProductoCarritoCompraPagado(int idProducto, string Usuario)
        {
            int total;
            Conector c = new Conector();
            c.Sql = "SELECT Cantidad as S from pedido where Usuario='" + Usuario + "' and Estado='False' and IdProducto="+idProducto;
            total = c.TotalPagar();
            return total;
        }

        //ACTUALIZAR STOCK CON ID PRODUCTO Y CANTIDAD NUEVA
        public int ActualizarCantidadProducto(int id, int stock)
        {
            int resultado = -1;
            Conector con = new Conector();
            con.Sql = "update producto set stock="+stock+" where Id="+id+"";
            resultado = con.ActualizarPedidos();
            return resultado;
        }

        //END

        public int ActualizarPedidos(string usuario)
        {
            int resultado = -1;
            Conector con = new Conector();
            con.Sql = "update pedido set Estado=1 where Usuario='" + usuario + "'";
            resultado = con.ActualizarPedidos();
            return resultado;
        }

        public int ActualizarPedidosRetiro(string usuario, string retiro)
        {
            int resultado = -1;
            Conector con = new Conector();
            con.Sql = "update pedido set Retiro='"+retiro+"' where Usuario='" + usuario + "' and Estado=0";
            resultado = con.ActualizarPedidosRetiro();
            return resultado;
        }

        public int ActualizarPedidosDireccion(string usuario, string direccion)
        {
            int resultado = -1;
            Conector con = new Conector();
            con.Sql = "update pedido set Direccion='" + direccion + "' where Usuario='" + usuario + "' and Retiro='Despacho'";
            resultado = con.ActualizarPedidosDireccion();
            return resultado;
        }

        public int CrearUsuario(string Usuario, string contraseña, string nombre, string apellido, string email, int fono, string direccion)
        {
            int resultado = -1;
            Conector con = new Conector();
            con.Sql = "INSERT INTO USUARIO VALUES ('"+Usuario+"','"+ contraseña + "','"+ nombre + "','"+ apellido + "','"+ email + "',"+ fono + ",'"+ direccion + "')";
            resultado = con.CrearUsuario();
            return resultado;
        }


        //SERVICIOS DESKTOP NETBEANS

        public string ContraseñaAdmin(string usuario)
        {
            if (usuario == string.Empty)
                return "";
            String contraseña = "";
            Conector c = new Conector();
            c.Sql = "SELECT contraseña as C FROM admin " +
                    " WHERE usuario = '" + usuario + "'";
            contraseña = c.ContraseñaAdmin();
            return contraseña;
        }


        public List<producto> ListaProductos()
        {
            List<producto> productos = new List<producto>();
            producto producto;
            Conector con = new Conector();
            con.Sql = "Select * from producto";
            DataSet ds = con.ListaProductos();
            foreach (DataRow fila in ds.Tables[0].Rows)
            {
                producto = new producto();
                producto.Id = Int32.Parse(fila["Id"].ToString());
                producto.Nombre = fila["nombre"].ToString();
                producto.Categoria = Convert.ToInt32(fila["categoria"].ToString());
                producto.Destino = Int32.Parse(fila["destino"].ToString());
                producto.Precio = Int32.Parse(fila["precio"].ToString());
                producto.Stock = Convert.ToInt32(fila["stock"].ToString());
                producto.Imagen = fila["imagen"].ToString();
                productos.Add(producto);
            }
            return productos;
        }

        public int CountCategoria()
        {
            int total;
            Conector c = new Conector();
            c.Sql = "SELECT count(id) as S FROM categoria";
            total = c.CountCategoria();
            return total;
        }

        public string NombreCategoria(string nombreProducto)
        {
            if (nombreProducto == string.Empty)
                return "";
            String nombre = "";
            Conector c = new Conector();
            c.Sql = "select categoria.nombre as N from producto join categoria on producto.categoria=categoria.Id " +
                    " WHERE producto.nombre = '"+nombreProducto+"'";
            nombre = c.NombreProducto();
            return nombre;
        }

        public string NombreCategoriaPorId(int id)
        {
            if (id < 1)
                return "";
            String nombre = "";
            Conector c = new Conector();
            c.Sql = "SELECT NOMBRE as N FROM categoria " +
                    " WHERE ID = " + id;
            nombre = c.NombreCategoriaPorId();
            return nombre;
        }

        //DESTINO

        public string NombreDestino(string nombreProducto)
        {
            if (nombreProducto == string.Empty)
                return "";
            String nombre = "";
            Conector c = new Conector();
            c.Sql = "select destino.nombre as N from producto join destino on producto.destino=destino.Id " +
                    " WHERE producto.nombre = '" + nombreProducto + "'";
            nombre = c.NombreDestino();
            return nombre;
        }

        public int CountDestino()
        {
            int total;
            Conector c = new Conector();
            c.Sql = "SELECT count(id) as S FROM destino";
            total = c.CountCategoria();
            return total;
        }

        public string NombreDestinoPorId(int id)
        {
            if (id < 1)
                return "";
            String nombre = "";
            Conector c = new Conector();
            c.Sql = "SELECT NOMBRE as N FROM destino " +
                    " WHERE ID = " + id;
            nombre = c.NombreDestinoPorId();
            return nombre;
        }

        public int PrecioProductoPorNombre(string nombre)
        {
            if (nombre==string.Empty)
                return 0;
            int precio;
            Conector c = new Conector();
            c.Sql = "SELECT PRECIO as P FROM producto " +
                    " WHERE nombre = '" + nombre + "'";
            precio = c.PrecioProductoPorNombre();
            return precio;
        }

        public int StockProductoPorNombre(string nombre)
        {
            if (nombre == string.Empty)
                return 0;
            int stock;
            Conector c = new Conector();
            c.Sql = "SELECT STOCK as S FROM producto " +
                    " WHERE nombre = '" + nombre + "'";
            stock = c.StockProductoPorNombre();
            return stock;
        }

        public string ImagenProductoPorNombre(string nombre)
        {
            if (nombre == string.Empty)
                return "";
            String imagen = "";
            Conector c = new Conector();
            c.Sql = "SELECT imagen as N FROM producto " +
                    " WHERE nombre = '"+nombre+"'";
            imagen = c.ImagenProductoPorNombre();
            return imagen;
        }

        public int ActualizarProducto(string nombreI, string nombreF, int categoria, int destino, int precio, int stock, string imagen)
        {
            int resultado = -1;
            Conector con = new Conector();
            con.Sql = "update producto set nombre='"+nombreF+"', categoria="+categoria+", destino="+destino+", precio="+precio+", stock="+stock+", imagen='"+imagen+"' where nombre='"+nombreI+"'";
            resultado = con.ActualizarProducto();
            return resultado;
        }

        public int IdCategoriaPorNombre(string nombre)
        {
            if (nombre == string.Empty)
                return 0;
            int id;
            Conector c = new Conector();
            c.Sql = "SELECT id as P FROM categoria " +
                    " WHERE nombre = '" +nombre+"'";
            id = c.IdCategoriaPorNombre();
            return id;
        }

        public int IdDestinoPorNombre(string nombre)
        {
            if (nombre == string.Empty)
                return 0;
            int id;
            Conector c = new Conector();
            c.Sql = "SELECT id as P FROM destino " +
                    " WHERE nombre = '" + nombre + "'";
            id = c.IdCategoriaPorNombre();
            return id;
        }

        public int EliminarProducto(string nombre)
        {
            int resultado = -1;
            Conector con = new Conector();
            con.Sql = "delete from producto where nombre='"+nombre+"'";
            resultado = con.EliminarProducto();
            return resultado;
        }

        public int UltimoProducto()
        {
            int ultimaId;
            Conector c = new Conector();
            c.Sql = "select top 1 id as P from producto order by id desc";
            ultimaId = c.UltimoProducto();
            return ultimaId;
        }

        public int InsertarProducto(int id, string nombre, int categoria, int destino, int precio, int stock, string imagen)
        {
            int resultado = -1;
            Conector con = new Conector();
            con.Sql = "insert into producto values("+id+",'"+nombre+"',"+categoria+","+destino+","+precio+","+stock+",'"+imagen+"')";
            resultado = con.InsertarProducto();
            return resultado;
        }

        public int InsertarCategoria(int id, string nombre)
        {
            int resultado = -1;
            Conector con = new Conector();
            con.Sql = "insert into categoria values(" + id + ",'" + nombre + "')";
            resultado = con.InsertarCategoria();
            return resultado;
        }

        public int EliminarCategoria(string nombre)
        {
            int resultado = -1;
            Conector con = new Conector();
            con.Sql = "delete from categoria where nombre='" + nombre + "'";
            resultado = con.EliminarCategoria();
            return resultado;
        }

        //LISTADO PEDIDOS

        public List<Pedido> ListaPedido()
        {
            List<Pedido> pedidos = new List<Pedido>();
            Pedido pedido;
            Conector con = new Conector();
            con.Sql = "Select * from pedido";
            DataSet ds = con.ListaPedido();
            foreach (DataRow fila in ds.Tables[0].Rows)
            {
                pedido = new Pedido();
                pedido.Id = Int32.Parse(fila["Id"].ToString());
                pedido.IdProducto = Int32.Parse(fila["IdProducto"].ToString());
                pedido.Usuario = fila["Usuario"].ToString();
                pedido.Cantidad = Int32.Parse(fila["Cantidad"].ToString());
                pedido.Total = Int32.Parse(fila["Total"].ToString());
                pedido.Estado = Convert.ToBoolean(fila["Estado"].ToString());
                pedido.Retiro = fila["Retiro"].ToString();
                pedido.Direccion = fila["Direccion"].ToString();
                pedidos.Add(pedido);
            }
            return pedidos;
        }

        public int CountPedido()
        {
            int total;
            Conector c = new Conector();
            c.Sql = "SELECT count(id) as S FROM pedido";
            total = c.CountPedido();
            return total;
        }

        //LISTA USUARIOS

        public List<usuario> ListaUsuarios()
        {
            List<usuario> usuarios = new List<usuario>();
            usuario usuario;
            Conector con = new Conector();
            con.Sql = "Select * from usuario";
            DataSet ds = con.ListaUsuarios();
            foreach (DataRow fila in ds.Tables[0].Rows)
            {
                usuario = new usuario();
                usuario.Nombre_usuario = fila["nombre_usuario"].ToString();
                usuario.Contraseña = fila["contraseña"].ToString();
                usuario.Nombre = fila["nombre"].ToString();
                usuario.Apellido = fila["apellido"].ToString();
                usuario.Email = fila["email"].ToString();
                usuario.Fono = Convert.ToInt32(fila["fono"].ToString());
                usuario.Direccion = fila["Direccion"].ToString();
                usuarios.Add(usuario);
            }
            return usuarios;
        }

        public int CountUsuario()
        {
            int total;
            Conector c = new Conector();
            c.Sql = "SELECT count(nombre_usuario) as S FROM usuario";
            total = c.CountUsuario();
            return total;
        }
    }
}
