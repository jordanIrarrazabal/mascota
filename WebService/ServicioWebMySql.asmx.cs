﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace WebService
{
    /// <summary>
    /// Descripción breve de ServicioWebMySql
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class ServicioWebMySql : System.Web.Services.WebService
    {

        [WebMethod]
        public string ContraseñaUsuarioBanco(int numero_cuenta)
        {
            string cadena = "server=localhost;uid=root;pwd=;database=banco;";
            MySqlConnection con = new MySqlConnection(cadena);
            con.Open();
            MySqlCommand cmd = new MySqlCommand("select contraseña from usuarios where numero_cuenta="+ numero_cuenta+"",con);
            string pass = Convert.ToString(cmd.ExecuteScalar());
            return pass;
            con.Close();            
        }

        [WebMethod]
        public int SaldoUsuarioBanco(int numero_cuenta)
        {
            string cadena = "server=localhost;uid=root;pwd=;database=banco;";
            MySqlConnection con = new MySqlConnection(cadena);
            con.Open();
            MySqlCommand cmd = new MySqlCommand("select saldo from usuarios where numero_cuenta=" + numero_cuenta + "", con);
            int saldo = Convert.ToInt32(cmd.ExecuteScalar().ToString());
            return saldo;
            con.Close();
        }

        [WebMethod]
        public int ActualizarSaldo(int nuevoSaldo, int numero_cuenta)
        {
            string cadena = "server=localhost;uid=root;pwd=;database=banco;";
            MySqlConnection con = new MySqlConnection(cadena);
            con.Open();
            MySqlCommand cmd = new MySqlCommand("update usuarios set saldo="+nuevoSaldo+ " where numero_cuenta=" + numero_cuenta + "", con);
            return cmd.ExecuteNonQuery();
            con.Close();
        }

        [WebMethod]
        public int InsertarPagos(int numeroCuenta, int cantidad)
        {
            string cadena = "server=localhost;uid=root;pwd=;database=banco;";
            MySqlConnection con = new MySqlConnection(cadena);
            con.Open();
            MySqlCommand cmd = new MySqlCommand("insert into pagos values(null,"+numeroCuenta+","+cantidad+")", con);
            return cmd.ExecuteNonQuery();
            con.Close();
        }
    }
}
