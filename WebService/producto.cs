﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace WebService
{
    public class producto
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Nombre { get; set; }
        [DataMember]
        public int Categoria { get; set; }
        [DataMember]
        public int Destino { get; set; }
        public int Precio { get; set; }
        [DataMember]
        public int Stock { get; set; }
        [DataMember]
        public string Imagen { get; set; }
        // contructor
        // metodos o funciones de la clase
    }
}