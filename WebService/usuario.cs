﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WebService
{
    public class usuario
    {
        [DataMember]
        public String Nombre_usuario { get; set; }
        [DataMember]
        public String Contraseña { get; set; }
        [DataMember]
        public String Nombre { get; set; }
        [DataMember]
        public String Apellido { get; set; }
        public String Email { get; set; }
        [DataMember]
        public int Fono { get; set; }
        [DataMember]
        public String Direccion { get; set; }
        // contructor
        // metodos o funciones de la clase
    }
}